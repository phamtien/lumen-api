<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function __construct()
    {
    }

    // Register
    public function register(RegisterRequest $request)
    {
        // Mới đăng ký thì không có mã token, refresh_token.
        $validate = $request->only(['name', 'email', 'password', 'token', 'refresh_token']);
        $validate['password'] = Hash::make($validate['password']);  // Tìm hiểu về mutators vs accessors để clean code.
        $validate['token'] = Str::random(40);
        $validate['refresh_token'] = Str::random(40);   // Mã tự sinh khi đăng nhập không cần tạo khi đăng ký
        $user = User::create($validate);
        $token = Str::random(40);   // Lặp code với $validate['token'] = Str::random(40); gây thừa code.
        Cache::put($token, 1, '300');   // Token là mã sinh ra 1 lần và duy nhất sao phải cache lại????
        $data = ['data' => $user, 'token' => $token];
        Mail::send('mail', $data, function ($msg) use ($user) {
            $msg->from('admin@gmail.com', 'admin');     // Nên config email vào file trong mục config và lấy ra từ .env. Không nên fix cứng vì deploy lên server sẽ sai.
            $msg->to($user->email, $user->name)->subject('Đăng ký tài khoản');
        });
        return response()->json(['status' => 201, "msg" => 'Một tệp thư đã gửi đến email của bạn hãy xác nhận và tiếp tục quá trình đăng ký'], 201);
    }

    // Đăng ký thành công tự động kích hoạt tài khoản không cần gọi thêm api kích hoạt.
    // kích hoạt tài khoản với active = 1
    public function accuracy($id, $token)
    {
        if ($id && Cache::get($token)) {
            $user = User::where('id', $id)->first();
            if ($user) {
                $user->active = 1;
                $user->save();
                Cache::forget($token);  // kích hoạt thành công xóa cache
                return response()->json(['status' => 200, "token" => $user->token], 200);
            }
        }
        return response()->json(['status' => 403, "message" => 'Phiên bản đã hết hạn'], 403);
    }

    // Lấy tất cả các users
    public function index(Request $request)
    {
        $limit = $request->get('limit', 10);
        $sort = $request->get('sort', 'asc');
        $search = $request->get('search', null);
        $user = User::where('name', 'like', '%' . $search . '%')->orderBy('id', $sort)->paginate($limit);
        return response()->json(['status' => 200, "data" => $user], 200);
    }

    // Đăng ký user (theo cách bình thường)
    public function store(RegisterRequest $request)
    {
        $validate = $request->only(['name', 'email', 'password', 'token', 'refresh_token']);
        $validate['password'] = Hash::make($validate['password']);
        $validate['token'] = Str::random(40);
        $validate['refresh_token'] = Str::random(40);
        $validate['active'] = 1 ;
        $user = User::create($validate);
        return response()->json(['status' => 201, "data" => $user], 201);
    }

    // Lấy ra 1 user
    public function show(Request $request)
    {
        $token = $request->header('token');
        if ($token) {
            $user = User::where('token', $token)->first();
            if ($user) {
                return response()->json(['status' => 200, "data" => $user], 200);
            }
        }
        return response()->json(['status' => 401, "message" => 'Bạn không có quyền truy cập'], 401);
    }


    // Cập nhật user
    public function update(UpdateUserRequest $request)
    {
        $token = $request->header('token');
        $validate = $request->only(['name', 'password']);
        $validate['password'] = Hash::make($validate['password']);
        if ($token) {
            $user = User::where('token', $token)->first();
            if ($user) {
                $user->update($validate);
                return response()->json(['status' => 200, "data" => $user, 'message' => 'Sửa thành công'], 200);
            }
        }
        return response()->json(['status' => 401, "message" => "Bạn không có quyền truy cập"], 401);
    }

    // Xóa tài khoản
    public function destroy(Request $request)
    {
        $token = $request->header('token');
        if ($token) {
            $user = User::where('token', $token)->first();
            if ($user) {
                $user->delete();
                return response()->json(['status' => 200, "message" => 'Xóa tài khoản thành công'], 200);
            }
        }
        return response()->json(['status' => 401, "message" => "Bạn không có quyền truy cập"], 401);
    }
}
