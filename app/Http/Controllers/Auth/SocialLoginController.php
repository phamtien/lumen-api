<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Core\Traits\ApiResponse;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    use ApiResponse;

    //google
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->stateless()->redirect();
    }

    public function processLoginGoogle()
    {
        $googleUser =  Socialite::driver('google')->stateless()->user();

        if(!$googleUser){
            return $this->error('user not found', 404);
        }
        $user = User::where('email', $googleUser->email)->first();


        if(!$user){
            $user = User::create([
                'name' => $googleUser->name,
                'email' => $googleUser->email,
                'social_id' => $googleUser->id,
            ]);
        }
        return $this->success($user, 200);
    }

    //facebook
    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    public function processLoginFacebook()
    {
        $facebookUser =  Socialite::driver('facebook')->stateless()->user();
        if(!$facebookUser){
            return $this->error('user not found', 404);
        }
        $user = User::where('email', $facebookUser->email)->first();
        // Chưa xử lý trường hợp email = null. Trong trường hợp có nhiều tài khoản email = null thì theo code trên sẽ mặc định lấy user đầu tiên. Với đăng nhập qua social cần check social id.


        if(!$user){
            $user = User::create([
                'name' => $facebookUser->name,
                'email' => $facebookUser->email,
                'social_provider' => 'facebook',
                'social_id' => $facebookUser->id,
            ]);
        }
        return $this->success($user, 200);
    }
}
