<?php

namespace App\Core\Traits;

trait ApiResponse
{
    public function success($data, $code)
    {
        return response()->json([
            'error' => false,
            'status' => $code,
            'data' => $data,
        ]);
    }

    public function error($message, $code)
    {
        return response()->json([
            'error' => true,
            'status' => $code,
            'message' => $message,
        ]);
    }
}
