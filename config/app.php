<?php
return [
    'providers' => [
        Laravel\Socialite\SocialiteServiceProvider::class
    ],
    'aliases' => [
        'Socialite' => Laravel\Socialite\Facades\Socialite::class
    ]
];
