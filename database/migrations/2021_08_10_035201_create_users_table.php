<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();  // Nếu không để mặc định là null sẽ gặp lỗi trường hợp đăng nhập qua social mà email = null.
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('token')->nullable()->unique();
            $table->string('refresh_token')->nullable()->unique();
            $table->bigInteger('active')->default(0);
            $table->string('social_provider')->default('google');
            $table->string('social_id')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
