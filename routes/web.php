<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return redirect()->route('swagger-lume.api');
});

//======== Các route thuộc cùng 1 nhóm nên gom lại vào group cho dễ kiểm soát =============

// Đăng ký tài khoản kích hoạt qua email
$router->post('/register', 'AuthController@register');

// Xác thực email để active = 1
$router->get('/accuracy/{id}/{token}', 'AuthController@accuracy');

// Lấy tất cả users
$router->get('/index', 'AuthController@index');

// Thêm 1 user
$router->post('/store', 'AuthController@store');

// Lấy 1 user bằng header token
$router->get('/show', 'AuthController@show');

// Update thông qua header token
$router->put('/update', 'AuthController@update');

// Delete token bằng header token
$router->delete('/destroy', 'AuthController@destroy');

//google
$router->get('login/google', 'Auth\SocialLoginController@redirectToGoogle');
$router->get('social-login/google', 'Auth\SocialLoginController@processLoginGoogle');

//facebook
$router->get('login/facebook', 'Auth\SocialLoginController@redirectToFacebook');
$router->get('social-login/facebook', 'Auth\SocialLoginController@processLoginFacebook');
